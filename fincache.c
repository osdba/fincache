#undef   _FILE_OFFSET_BITS
#define  _FILE_OFFSET_BITS  64

#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h> 

#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>

#include <stdarg.h>

int usage()
{
    const char * usageinfo=
"fincache version 0.1\n" \
"Usage:   fincache [OPTION] <filename>\n" \
"display file how may page cache in memory.\n\n" \
"  -v \n"\
"       in detail mode\n"\
"  -c \n"\
"       clear cache\n"\
"  -h\n"\
"       help\n"\
"Example:\n"\
"   fincache /tmp/myfile.txt\n"\
"       This command display /tmp/myfile.txt how many page cache in memory.\n"\
"   fincache -c /tmp/myfile.txt\n"\
"       This command clear file /tmp/myfile.txt cache memory\n";
    printf("%s\n", usageinfo);
    return 0;
}

int clear_file_cache(const char *filename)  
{  
    struct stat st;  
    if(stat(filename , &st) < 0)
    {  
        fprintf(stderr , "stat localfile failed, path:%s\n",filename);  
        return -1;  
    }  
  
    int fd = open(filename, O_RDONLY);  
    if( fd < 0 )
    {  
        fprintf(stderr , "open localfile failed, path:%s\n",filename);  
        return -1;  
    }  
  
    //clear cache by posix_fadvise  
    if( posix_fadvise(fd,0,st.st_size,POSIX_FADV_DONTNEED) != 0)
    {  
        printf("Cache FADV_DONTNEED failed, %s\n",strerror(errno));  
    }  
    else
    {  
        printf("Cache FADV_DONTNEED done\n");  
    }  
  
    return 0;  
}  


void fincore(char *filename, int display_detail)
{
   int fd;
   struct stat st;
   void *pa = (char *)0;
   char *vec = (char *)0;
   int n = 0;
   int pageSize = getpagesize();
   int pageIndex;
   
   fd = open(filename, 0);
   if (0 > fd)
   {
      perror("open");
      return;
   }

   if (0 != fstat(fd, &st))
   {
      perror("fstat");
      close(fd);
      return;
   }

   pa = mmap((void *)0, st.st_size, PROT_NONE, MAP_SHARED, fd, 0);
   if (MAP_FAILED == pa)
   {
      perror("mmap");
      close(fd);
      return;
   }

   /* vec = calloc(1, 1+st.st_size/pageSize); */
   vec = calloc(1, (st.st_size+pageSize-1)/pageSize);
   if ((void *)0 == vec)
   {
      perror("calloc");
      close(fd);
      return;
   }

   if (0 != mincore(pa, st.st_size, vec))
   {
      /* perror("mincore"); */
      fprintf(stderr, "mincore(%p, %lu, %p): %s\n",
              pa, (unsigned long)st.st_size, vec, strerror(errno));
      free(vec);
      close(fd);
      return;
   }

   /* handle the results */
   for (pageIndex = 0; pageIndex <= st.st_size/pageSize; pageIndex++)
   {
       if (display_detail)
       {
		   if ( (pageIndex % 32 ) ==0 && pageIndex > 0)
		   {
			   printf("\n");
		   }
		   
		   if ( (pageIndex % 32 ) ==0)
		   {
			   printf("%08x : ", pageIndex);
		   } 
	   }
	   
	   if (vec[pageIndex]&1) 
	   {
		   if (display_detail)
		   {
		       printf("1");
		   }
		   n++;
	   }
	   else
	   {
		   if (display_detail)
		   {
		       printf("0");
		   }
	   }
   }
   
   if (display_detail)
   {
	   printf("\n");
   }

   free(vec);
   vec = (char *)0;

   munmap(pa, st.st_size);
   close(fd);
  
   printf("Total: %lld, %d in caches\n", (long long int)(st.st_size/pageSize), n); 
   return;
}

int main(int argc, char * argv[])
{	
	int display_detail=0;
	int is_clear_cache=0;
	char ch;
	char * filename = argv[1];
    
    if (argc < 2)
    {
        usage(argv[0]);
        return 0;
    }
	
	opterr=0;
    while ( (ch = getopt(argc,argv,"hv:c:")) != -1)
    {
        switch (ch)
        {
        case 'h':
            usage(argv[0]);
            return 0;
        case 'v':
            display_detail = 1;
            filename = optarg; 
            break;
            
        case 'c':
            is_clear_cache = 1;
            filename = optarg;
            break;
            
        default:
            usage(argv[0]);
            return 1;
        }
    }
    
    if (is_clear_cache)
    {
        clear_file_cache(filename);
    }
    else
    {
	    fincore(filename, display_detail);
	}
	return 0;
}
