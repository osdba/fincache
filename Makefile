CC=gcc
MAKE=make
CFLAGS=$(DEBUG) -static

fincache:
	$(CC) $(DEBUG) $(CFLAGS) fincache.c -o fincache

debug:
	$(MAKE) $(MAKEFILE) DEBUG="-g -DDEBUG"
clean: 
	if [ -f fincache ] ; then rm fincache; fi
