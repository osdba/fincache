## fincache介绍

有时，我们在测试时，需要把一个文件的cache清理掉，以便得到更准确的测试结果，就可以使用此工具。 

下载解压后，直接运行make就编译生成一个fincache的文件。

使用方法如下： 我们显示一个文件，有多少被cache了：
```
osdba@osdba-laptop:~/03.tangsrc/fincache$ ./fincache ~/tmp/t1.dat
Total: 12800, 0 in caches
```


上面显示这个文件的大小为“12800“页面，每个页面的大小为4k，0个页面被cache，说明这个文件没有被cache。 我们使用dd读一下这个文件，这个文件就会被cache：
```
osdba@osdba-laptop:~/03.tangsrc/fincache$ dd if=~/tmp/t1.dat of=/dev/null bs=1M
50+0 records in
50+0 records out
52428800 bytes (52 MB) copied, 0.209783 s, 250 MB/s
osdba@osdba-laptop:~/03.tangsrc/fincache$ ./fincache ~/tmp/t1.dat
Total: 12800, 12800 in caches
```

使用“-c”参数，清理一个cache，再看，这个文件就没有被cache住了：

```
osdba@osdba-laptop:~/03.tangsrc/fincache$ ./fincache -c  ~/tmp/t1.dat
Cache FADV_DONTNEED done
osdba@osdba-laptop:~/03.tangsrc/fincache$ ./fincache  ~/tmp/t1.dat
Total: 12800, 0 in caches
```

